# atlassian-jwt

![build-status](https://bitbucket-badges.atlassian.io/badge/atlassian/atlassian-jwt-js.svg)

[JWT (JSON Web Token)](http://self-issued.info/docs/draft-jones-json-web-token.html) encoding & decoding library for node.js. Built of [jwt-simple](https://github.com/hokaccha/node-jwt-simple) and adds Atlassian's custom QSH (query string hash) claim.

For more information on using JWT tokens with Atlassian add-ons, please read: [Understanding JWT](https://developer.atlassian.com/cloud/jira/platform/understanding-jwt/).

## Install

    $ npm install atlassian-jwt

## Usage

### Create a JWT token

```javascript
var jwt = require('atlassian-jwt');
var moment = require('moment'); // time library for convenience

var now = moment().utc();

// Simple form of [request](https://npmjs.com/package/request) object
var req = {
    method: 'GET',
    originalUrl: '/rest/resource/you/want'
};

var token = {
    "iss": 'issuer-val',
    "iat": now.unix(),                      // the time the token is generated
    "exp": now.add(3, 'minutes').unix(),    // token expiry time (recommend 3 minutes after issuing)
    "qsh": jwt.createQueryStringHash(req)   // [Query String Hash](https://developer.atlassian.com/cloud/jira/platform/understanding-jwt/#a-name-qsh-a-creating-a-query-string-hash)
};

var secret = 'xxx';

var token = jwt.encode(token, secret);
console.log(token);
```

### Decode a JWT token

```javascript
/*
 * jwt.decode(token, secret, noVerify, algorithm)
 *
 * Decodes the JWT token and verifies the signature using the secret and algorithm. Algorithm defaults to HS256.
 */
var decoded = jwt.decode(token, secret);
console.log(decoded); //=> { foo: 'bar' }

/*
 * Decode without verifing the signature of the token.
 * Tokens should never be used without verifying the signature as otherwise payload trust cannot be established.
 */
var decoded = jwt.decode(token, null, true);
console.log(decoded); //=> { foo: 'bar' }
```

### Miscellaneous Utilities

`jwt.createQueryStringHash(req, checkBodyForParams, baseUrl)` - Create a QSH using the algorithm defined by [the algorithm](https://developer.atlassian.com/static/connect/docs/latest/concepts/understanding-jwt.html#qsh) 
`jwt.createCanonicalRequest(req, checkBodyForParams, baseUrl)` - Creates a canonical request which is used to calculate the QSH for the JWT token. Prefer using #createQueryStringHash() directly

### Algorithms

By default the algorithm to encode is `HS256`.

The supported algorithms for encoding and decoding are `HS256`, `HS384`, `HS512` and `RS256`.

```javascript
// encode using HS512
jwt.encode(payload, secret, 'HS512')
```